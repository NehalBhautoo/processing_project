import processing.core.PApplet;
import processing.core.PShape;

/**
 * AppLet class load 3D model
 * @author Nehal Bhautoo
 */
public class AppLet extends PApplet {

    private PShape models;

    public static void main(String[] args) {
        PApplet.main("AppLet");
    }

    @Override
    public void settings() {
        fullScreen(P3D);
    }

    @Override
    public void setup() {
        // LOAD models
        models = loadShape("models/star_war_fighter.obj");
        smooth();
        noStroke();
    }

    @Override
    public void draw() {
        background(0);
        lights();
        shape(models, 500, 400);
    }

    public void lights() {
        int w=170, m=-422, p=+422;
        ambientLight(w,w,w);
        directionalLight(w, w, w, m, m, m);
        directionalLight(w, w, w, p, p, p);
        directionalLight(w, w, w, m, m, p);
        directionalLight(w, w, w, p, p, m);
    }
}
