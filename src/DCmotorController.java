import processing.serial.Serial;

import java.awt.*;

public class DCmotorController extends AppLet {
    Serial port;
    boolean rectOver = false;
    int rectX, rectY;                      // Position of square button
    int rectSize = 100;                     // Diameter of rect
    Color rectColor;
    boolean buttonOn = false;              // Status of the button

    @Override
    public void setup() {
        size(200, 200);
        noStroke();
        frameRate(10);
        rectColor = new Color(100);
        rectX = width/2 - rectSize/2;
        rectY = height/2 - rectSize/2;
        // Open the port that the board is connected to and use the same speed (9600 bps)
        port = new Serial(this, 9600);
    }

    @Override
    public void draw() {
        update(mouseX, mouseY);
        background(0);                 // Clear background to black
        rect(rectX, rectY, rectSize, rectSize);
    }

    void update(int x, int y) {
        rectOver = overRect(rectX, rectY, rectSize, rectSize);
    }

    boolean overRect(int x, int y, int width, int height) {
        return (mouseX >= x) && (mouseX <= x + width) &&
                (mouseY >= y) && (mouseY <= y + height);
    }

    @Override
    public void mouseReleased() {
        if (rectOver) {
            if (buttonOn) {
                rectColor = new Color(100);
                buttonOn = false;
                port.write('L');           // Send an L to indicate button is OFF
            } else {
                rectColor = new Color(180);
                buttonOn = true;
                port.write('H');           // Send an H to indicate button is ON
            }
        }
    }
}
