import processing.core.PApplet;
import processing.serial.*;

/**
 * Implementation of a servo controller,
 * connected to an arduino
 * @author Nehal Bhautoo
 */

public class ServoController extends PApplet {
    Serial port;
    float mx = (float) 0.0;

    @Override
    public void setup() {
        size(200, 200);
        noStroke();
        frameRate(10);
        port = new Serial(this, 9600);
    }

    @Override
    public void draw() {
        background(0);
        fill(204);
        rect(40, height/2-15, 120, 25);

        float dif = mouseX - mx;
        if (abs(dif) > 1.0) {
            mx += dif/4.0;
        }

        mx = constrain(mx, 50, 149);                // Keeps marker on the screen
        noStroke();
        fill(255);
        rect(50, (height/2)-5, 100, 5);
        fill(204, 102, 0);

        rect(mx-2, height/2-5, 4, 5);                              // Draw the position marker
        int angle = (int) map(mx, 50, 146, 0, 180);   // Scale the value to the range 0-180
        print(angle + " ");	                                                    // Print the current angle (debug)
        port.write(angle);	                                                    // Write the angle to the serial port
    }
}
